-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Bulan Mei 2022 pada 14.38
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_desa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `fasilitas2`
--

CREATE TABLE `fasilitas2` (
  `id` int(11) NOT NULL,
  `no` int(25) DEFAULT NULL,
  `jenisprasarana` varchar(255) DEFAULT NULL,
  `pembiayaan` varchar(255) DEFAULT NULL,
  `volume` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `fasilitas2`
--

INSERT INTO `fasilitas2` (`id`, `no`, `jenisprasarana`, `pembiayaan`, `volume`, `status`) VALUES
(22, 1, 'gudang', '10000000', '1000', 'berjalan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembangunan`
--

CREATE TABLE `pembangunan` (
  `id` int(11) NOT NULL,
  `no` int(30) NOT NULL,
  `uraiankegiatan` varchar(255) NOT NULL,
  `anggaran` int(255) NOT NULL,
  `realisasianggaran` int(255) NOT NULL,
  `sisaanggaran` int(255) NOT NULL,
  `tahunanggaran` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pembangunan`
--

INSERT INTO `pembangunan` (`id`, `no`, `uraiankegiatan`, `anggaran`, `realisasianggaran`, `sisaanggaran`, `tahunanggaran`) VALUES
(4, 1, 'rwg', 12, 2000000, 12, 2019);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penduduk`
--

CREATE TABLE `penduduk` (
  `id_penduduk` int(11) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama_Penduduk` varchar(45) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `rt` varchar(3) NOT NULL,
  `rw` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penduduk`
--

INSERT INTO `penduduk` (`id_penduduk`, `nik`, `nama_Penduduk`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `rt`, `rw`) VALUES
(1, '136513513513', 'sukijann', 'jakarta', '1985-02-26', 'L', '001', '014'),
(2, '1365136122', 'suki', 'bandung', '1995-12-26', 'P', '002', '013'),
(3, '232424242', 'sukiman', 'bogor', '1945-02-20', 'L', '002', '011'),
(4, '31515135', 'sukimana', 'yogya', '2002-02-26', 'P', '012', '004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `password`, `level`) VALUES
(1, 'MUHAMMAD BINTANG ELAN YUSYAFI', 'bintangelan', '123456', 'admin'),
(2, 'ADRIKNI ALHAMD', 'adrikni', '654321', 'Pegawai'),
(3, 'ADZKIA QURROTA AYUNI', 'adzkia', '111111', 'pegawai');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `fasilitas2`
--
ALTER TABLE `fasilitas2`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pembangunan`
--
ALTER TABLE `pembangunan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penduduk`
--
ALTER TABLE `penduduk`
  ADD PRIMARY KEY (`id_penduduk`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `fasilitas2`
--
ALTER TABLE `fasilitas2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `pembangunan`
--
ALTER TABLE `pembangunan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
